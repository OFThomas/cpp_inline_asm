/**
 * @author      : oli (oli@oli-desktop)
 * @file        : main
 * @created     : Monday Oct 03, 2022 20:48:15 BST
 */

#include <iostream>

#include <cmath>

#include <olistd/timer>


int main()
{

    std::cout << "hi" << std::endl;

    const double val = 0.4;
    olistd::Timer time;

    const double ans = std::sqrt(val);
    time.stop();
    std::cout << ans << std::endl;


    
    return 0;
}

